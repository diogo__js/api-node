import { ISpecificationsRepository } from '../../repositories/ISpecificationsReporitory'

interface IRerquest {
	name: string
	description: string
}

class CreateSpecificationUseCase {

	constructor(private specificationsRepository: ISpecificationsRepository){}

	execute({ name, description }: IRerquest):void {
        
		const specificationAllReadyExists = this.specificationsRepository.findByName(name)

		if(specificationAllReadyExists){
			throw new Error('Speciifcation Already exists')
		}


		this.specificationsRepository.create({ name, description })

	}

}

export { CreateSpecificationUseCase }